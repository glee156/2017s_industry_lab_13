package ictgradschool.industry.lab13.ex01;

import ictgradschool.Keyboard;

/**
 * Created by glee156 on 12/12/2017.
 */
public class ExerciseOne {

    public void start() {
        SimpleRunnable myRunnable = new SimpleRunnable();
        Thread thread = new Thread(myRunnable);
        thread.start();
//        System.out.println("Press ENTER to stop the thread...");
//        Keyboard.readInput();

        //while loop is also possible here to interrupt the thread instead of in the runnable class
        //interrupting the thread here instead of in the runnable allows you to pass on info about thread to other threads
        /*while (runnable.getCounter() < 100){

        }
        thread.interrupt();*/

    }

    public static void main(String[] args) {
        ExerciseOne test = new ExerciseOne();
        test.start();
    }
}
