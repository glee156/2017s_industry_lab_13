package ictgradschool.industry.lab13.ex01;

/**
 * Created by glee156 on 12/12/2017.
 */
public class SimpleRunnable implements Runnable{
    private long counter = 0;

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            counter++;
            if(counter == 1000000){
                Thread.currentThread().interrupt();
            }
            System.out.println(counter);
        }
    }

    public long getCounter() {
        return counter;
    }
}
