package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.List;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {
    double total;
    int threadsNumber = 4;
    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples){
        // TODO Implement this.

        for(int i = 0; i < threadsNumber; i++){
            Thread thread = new Thread(new Runnable(){
                @Override
                public void run(){
                    ThreadLocalRandom tlr = ThreadLocalRandom.current();

                    long numInsideCircle = 0;

                    for (long i = 0; i < numSamples/threadsNumber; i++) {

                        double x = tlr.nextDouble();
                        double y = tlr.nextDouble();

                        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                            numInsideCircle++;
                        }

                    }

                    double estimatedPi = 4.0 * (double) numInsideCircle / (double) numSamples;
                    System.out.println(estimatedPi);
                    total += estimatedPi;
                }
            });

            thread.start();

            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return total;
    }

    /** Program entry point. */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
