package ictgradschool.industry.lab13.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
/**
 * Created by glee156 on 12/12/2017.
 */
public class ConcurrentBankingApp {
    public void start(){
        ArrayBlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);
        BankAccount account = new BankAccount();

        Thread consumerOne = new Thread(new Consumer(queue, account));
        Thread consumerTwo = new Thread(new Consumer(queue, account));

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                //TransactionGenerator.writeDataFile(20);
                List<Transaction> transactions = TransactionGenerator.readDataFile();

                for(Transaction t : transactions){
                    try {
                        queue.put(t);
                        //System.out.println(queue.size());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        producer.start();
        consumerOne.start();
        consumerTwo.start();

        try {
            producer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        consumerOne.interrupt();
        consumerTwo.interrupt();

        try {
            consumerOne.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            consumerTwo.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Print the final balance after applying all Transactions.
        System.out.println("Final balance: " + account.getFormattedBalance());
    }

    public static void main(String[] args) {
        new ConcurrentBankingApp().start();
    }

}
