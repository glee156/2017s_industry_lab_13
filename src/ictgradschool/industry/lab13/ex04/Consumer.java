package ictgradschool.industry.lab13.ex04;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by glee156 on 12/12/2017.
 */
public class Consumer implements Runnable{
    public ArrayBlockingQueue<Transaction> queue;
    public BankAccount account;
    private final Lock lock = new ReentrantLock();

    Consumer(ArrayBlockingQueue queue, BankAccount account){
        this.queue = queue;
        this.account = account;
    }

    public void run(){
        try {
            //lock.lock();

            //take Transaction object from queue
            while(!Thread.currentThread().isInterrupted()) {
                    Transaction current = queue.take();

                    //execute either deposit or withdrawal on BankAccount object
                    transact(current);
                //System.out.println(Thread.currentThread().isInterrupted());
            }

        } catch (InterruptedException e) {
            //e.printStackTrace();
        } finally{
            //lock.unlock();
            if(Thread.currentThread().isInterrupted()){
                while(queue.size() > 0){
                    //System.out.println("interrupted");
                    //System.out.println(queue.size());
                    Transaction t = queue.poll();
                    transact(t);
                }
                System.out.println(queue.isEmpty());
            }
        }
    }

    public void transact(Transaction transaction){
        //ensures only one thread modifies the account object at a time cos if 2 threads try to modify the object at the same time, order of which modification gets executed first may change every time
            synchronized (account) {
                switch (transaction._type) {
                    case Deposit:
                        //System.out.println(" Depositing " + transaction._amountInCents);
                        account.deposit(transaction._amountInCents);
                        break;
                    case Withdraw:
                        //System.out.println(" Withdrawing " + transaction._amountInCents);
                        account.withdraw(transaction._amountInCents);
                        break;
                }
            }
    }
}
